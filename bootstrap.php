<?php
/**
 * Pageless Buttons Plugin
 *
 * @package   ThoriumDesign\PagelessButtons
 * @since     1.0.0
 * @author    ThoriumDesign
 * @link      https://thoriumdesign.com
 * @contributors ajskelton, jtilford
 *
 * @wordpress-plugin
 * Plugin Name: Pageless Buttons
 * Plugin URI: _
 * Description: Pageless Buttons is a WordPress plugin that creates clickable buttons that open to the full screen
 * Version: 1.2.6
 * Author: ThoriumDesign
 * Author URI: https://thoriumdesign.com
 * Text Domain: pageless_buttons
 * Requires WP: 4.7
 * Requires PHP: 5.5
 * Bitbucket Plugin URI: https://bitbucket.org/thoriumdesign/pageless-buttons
 * Bitbucket Branch: master
 *
 */
namespace ThoriumDesign\PagelessButtons;

if ( ! defined( 'ABSPATH' ) ) {
	exit( "Oh, silly, there's nothing to see here." );
}

define( 'PAGELESS_BUTTONS_PLUGIN', __FILE__ );
define( 'PAGELESS_BUTTONS_DIR', plugin_dir_path( __FILE__ ) );
$plugin_url = plugin_dir_url( __FILE__ );
if( is_ssl() ) {
	$plugin_url = str_replace( 'http://', 'https://', $plugin_url );
}
define( 'PAGELESS_BUTTONS_URL', $plugin_url );
define( 'PAGELESS_BUTTONS_TEXT_DOMAIN', 'pageless_buttons' );
define( 'PAGELESS_BUTTONS_VERSION', '1.0.0' );

include( __DIR__ . '/src/plugin.php' );

