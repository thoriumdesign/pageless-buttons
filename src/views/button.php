<?php d($attributes) ?>

<div style="position: relative;">
<div class="morph-button morph-button-modal morph-button-modal-2 morph-button-fixed">
	<?php if ( !empty($iconL) ) { ?>
	<button type="button" class="et_pb_button left-icon" data-icon="'.$iconL.'">' . $title . '</button>
	<?php } else { ?>
	<button type="button" class="et_pb_button"><?php echo $attributes['title'] ?></button>
	<?php } ?>
	<div class="morph-content">
		<div>
			<div class="content-style-form content-style-form-1">
				<span class="icon icon-close">Close the dialog</span>
				<?php if ( !empty($notitle) ) { ?>
				<h2><?php echo $attributes['title'] ?></h2>
				<?php } ?>
				<div class="morph-content-body"><?php echo do_shortcode( $content ) ?></div>
				</div>
			</div>
		</div>
	</div>
</div>