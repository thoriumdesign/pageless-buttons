<div class="blurb__wrap">
    <div data-modal="#<?php echo $attributes['slug']; ?>"  class="modal__trigger">
        <?php echo (wp_get_attachment_image( $image_id, 'medium')); ?>
        <div class="blurb">
            <h4><?php echo sanitize_text_field($attributes['button-title']); ?></h4>
            <p><?php echo sanitize_text_field($attributes['button-desc']); ?></p>
            <div class="blurb-meta">
                <span class="meta-category"><?php echo sanitize_text_field($attributes['category']); ?></span>
                <span class="read-more">READ MORE</span>
            </div>
        </div>

    </div>

    <div id="<?php echo $attributes['slug']; ?>" class="modal modal__bg" role="dialog" aria-hidden="true">
        <div class="modal__dialog">
            <div class="modal__content">
                <div class="modal__content-image background-image-defer" style="
                    background-position:<?php echo sanitize_text_field($attributes['background-position']); ?>;
                    height: <?php echo sanitize_text_field($attributes['background-height']) . 'vh' ?>;
                    " data-src="<?php echo sanitize_text_field(wp_get_attachment_image_url( $image_id, 'full' )); ?>"></div>
                <div class="modal__content-text">
                    <h2><?php the_title(); ?></h2>
                    <?php do_shortcode(the_content()); ?>
                </div>
            </div>
            <a href="" class="modal__close">
                <svg class="" viewBox="0 0 24 24"><path d="M19 6.41l-1.41-1.41-5.59 5.59-5.59-5.59-1.41 1.41 5.59 5.59-5.59 5.59 1.41 1.41 5.59-5.59 5.59 5.59 1.41-1.41-5.59-5.59z"/><path d="M0 0h24v24h-24z" fill="none"/></svg>
            </a>
        </div>
    </div>
</div>