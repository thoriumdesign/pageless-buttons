<?php
// We need to create a unique class, so we can assign the image to the :before element of the button
// Remove whitespace from name, replace with nothing
$title_clean = preg_replace('/[^a-z0-9]/i', '', $title);
// Make all characters lowercase
$title_clean = strtolower($title_clean);
$classtitle = 'ag-mb-'.$title_clean;
?>
<div style="position: relative;">
    <span class="svg-defs"><?php ThoriumDesign\PagelessButtons\include_svg_icons(); ?></span>
    <div class="morph-button morph-button-overlay morph-button-portfolio morph-button-fixed">
		<button type="button"><h3 class="mb-name"><?php echo $attributes['title'] ?></h3><p class="mb-cat"><?php echo $attributes['description'] ?></p></button>
		<div class="morph-content">
			<div>
				<div class="content-style-overlay">
					<span class="icon icon-close">Close</span>
					<div class="morph-content-body"><?php echo do_shortcode($content) ?></div>
				</div>
			</div>
		</div>
	</div>
</div>