<?php
$title_clean = preg_replace('/[^a-z0-9]/i', '', $attributes['title']);
// Make all characters lowercase
$title_clean = strtolower($title_clean);
$class_title = 'ag-mb-'.$title_clean;
?>
<div style="position: relative;">
    <div class="morph-button morph-button-overlay morph-button-portfolio-card morph-button-fixed">
        <button type="button" class="<?php echo $class_title; ?>"><h3 class="mb-name"><?php echo $attributes['title'] ?></h3>
            <p class="mb-cat"><?php echo $attributes['description'] ?></p></button>
        <div class="morph-content">
            <div>
                <div class="content-style-overlay">
                    <span class="icon icon-close">Close</span>
                    <div class="morph-content-body"><?php echo do_shortcode($content) ?></div>
                </div>
            </div>
        </div>
    </div>
</div>
<style>.morph-button-portfolio > button.

    <?php echo $class_title ?>,
    :before {
        background-image: url("<?php echo $attributes['image_url'] ?>);
    }
</style>