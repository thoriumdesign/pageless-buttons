<div class="cd-section cd-form">

	<div class="cd-modal-action">
		<a href="#0" class="btn" data-size="<?php echo $attributes['type']; ?>" data-type="modal-trigger" data-text-color="<?php echo $attributes['text-color']; ?>" data-modal-color="<?php echo $attributes['modal-color']; ?>" style="background-color: <?php echo $attributes['button-color']; ?>; color: <?php echo$attributes['text-color']; ?>"><?php echo $attributes['title']; ?></a>
		<span class="cd-modal-bg" style="background-color: <?php echo $attributes['bg-color']; ?>"></span>
	</div>

	<div class="cd-modal">
		<div class="cd-modal-content">
			<?php echo do_shortcode( $content ) ?>
		</div> <!-- cd-modal-content -->
	</div> <!-- cd-modal -->

	<a href="#0" class="cd-modal-close">Close</a>

    <div class="cd-bg-overlay"></div>
</div>