<div style="position: relative;">
	<div class="morph-button morph-button-overlay morph-button-fixed">
		<button type="button"><?php echo $attributes['title']; ?></button>
		<div class="morph-content">
			<div>
				<div class="content-style-overlay">
					<span class="icon icon-close">Close the dialog</span>
					<?php if ( !empty($attributes['notitle']) ) { ?>
					<h2><?php echo $attributes['title'] ?></h2>
					<?php } ?>
					<div class="morph-content-body"><?php echo do_shortcode( $content ); ?></div>
				</div>
			</div>
		</div>
	</div>
</div>