<?php
use ThoriumDesign\PagelessButtons\Shortcode as Shortcode;
?>

<div class="blurb-three-col-grid">

    <?php
        Shortcode\loop_and_render_blurb($query, $attributes, $config);
    ?>

</div>