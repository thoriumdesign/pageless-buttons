<?php
/**
 * Shortcode processors
 *
 * @package   ThoriumDesign\PagelessButtons
 * @since     1.0.0
 * @author    ThoriumDesign
 * @link      https://thoriumdesign.com
 * @contributors ajskelton, jtilford
 */

namespace ThoriumDesign\PagelessButtons\Shortcode;


use function ThoriumDesign\PagelessButtons\get_attachment_id_from_url;

add_shortcode( 'pagelessbutton', __NAMESPACE__ . '\process_the_shortcode' );
/**
 * Process the Shortcode to build the button
 *
 * @since 1.0.0
 *
 * @param array $user_defined_attributes
 * @param string $content
 *
 * @return string
 */
function process_the_shortcode( $user_defined_attributes, $content, $shortcode_name ) {
	$config = get_pageless_button_configuration( $shortcode_name );

	$attributes = shortcode_atts(
		$config,
		$user_defined_attributes,
		$shortcode_name
	);


//	d( $content );
//	d( $attributes );

	ob_start();
	include( $config['views'][ $attributes['type'] ] );

	return ob_get_clean();
}

/**
 * Get the runtime configuration parameters for the specified shortcode
 *
 * @since 1.0.0
 *
 * @param string $shortcode_name Name of the shortcode
 *
 * @return array
 */
function get_pageless_button_configuration( $shortcode_name ) {
	$config = array(
		'views'        => array(
			'page'            => PAGELESS_BUTTONS_DIR . 'src/views/page.php',
			'button'          => PAGELESS_BUTTONS_DIR . 'src/views/button.php',
			'form'            => PAGELESS_BUTTONS_DIR . 'src/views/form.php',
			'slim-form'       => PAGELESS_BUTTONS_DIR . 'src/views/slim-form.php',
			'portfolio'       => PAGELESS_BUTTONS_DIR . 'src/views/portfolio.php',
			'portfolio-cards' => PAGELESS_BUTTONS_DIR . 'src/views/portfolio-cards.php',
			'redux'           => PAGELESS_BUTTONS_DIR . 'src/views/redux.php',
		),
		'text-color'   => '',
		'button-color' => '',
		'modal-color'  => '',
		'type'         => '',
		'title'        => '',
		'left-icon'    => '',
		'no-icon'      => '',
		'description'  => '',
		'image'        => ''
	);

	return $config;
}

add_shortcode( 'material-modal', __NAMESPACE__ . '\process_material_modal_shortcode' );
/**
 * Process the Shortcode to build the button
 *
 * @since 1.0.0
 *
 * @param array $user_defined_attributes
 * @param string $content
 *
 * @return string
 */
function process_material_modal_shortcode( $user_defined_attributes, $content, $shortcode_name ) {
	static $button_counter = 0;
	$button_counter ++;

	$config = get_material_modal_configuration( $shortcode_name );

	$attributes = shortcode_atts(
		$config,
		$user_defined_attributes,
		$shortcode_name
	);
	$image_id   = get_attachment_id_from_url( trailingslashit( get_site_url() ) . ltrim( sanitize_text_field( $attributes['image'] ), '/' ) );

	ob_start();

	include( $config['views'][ $attributes['type'] ] );

	return ob_get_clean();
}

add_shortcode( 'bio-modal', __NAMESPACE__ . '\process_bio_modal_shortcode' );
/**
 * Process the Shortcode to build the button
 *
 * @since 1.0.0
 *
 * @param array $user_defined_attributes
 * @param string $content
 *
 * @return string
 */
function process_bio_modal_shortcode( $user_defined_attributes, $content, $shortcode_name ) {
	static $button_counter = 0;
	static $modal_counter = 0;
	$button_counter ++;

	$config = get_material_modal_configuration( $shortcode_name );

	$attributes = shortcode_atts(
		$config,
		$user_defined_attributes,
		$shortcode_name
	);

	ob_start();

	include( $config['views'][ $attributes['type'] ] );

	return ob_get_clean();
}

/**
 * Get the runtime configuration parameters for the specified shortcode
 *
 * @since 1.0.0
 *
 * @param string $shortcode_name Name of the shortcode
 *
 * @return array
 */
function get_material_modal_configuration( $shortcode_name ) {
	$config = array(
		'views'               => array(
			'modal' => PAGELESS_BUTTONS_DIR . 'src/views/modal.php',
			'bio-modal' => PAGELESS_BUTTONS_DIR . 'src/views/bio-modal.php'
		),
		'text-color'          => '',
		'button-color'        => '',
		'modal-color'         => '',
		'type'                => '',
		'title'               => '',
		'left-icon'           => '',
		'no-icon'             => '',
		'description'         => '',
		'image'               => '',
		'id'                  => '',
		'background-position' => 'center',
		'background-height'   => '33',
		'button-title'        => '',
		'button-desc'         => '',
		'slug'                => '',
	);

	return $config;
}

add_shortcode( 'material-blurbs', __NAMESPACE__ . '\process_material_blurbs_shortcode' );

function process_material_blurbs_shortcode( $user_defined_attributes, $content, $shortcode_name ) {
	$config = get_material_blurb_configuration( $shortcode_name );

	$attributes = shortcode_atts(
		$config,
		$user_defined_attributes,
		$shortcode_name
	);

	ob_start();

	render_blurb_posts( $attributes, $config );

	return ob_get_clean();

}

/**
 * Get the runtime configuration parameters for the specified shortcode
 *
 * @since 1.0.0
 *
 * @param string $shortcode_name Name of the shortcode
 *
 * @return array
 */
function get_material_blurb_configuration( $shortcode_name ) {
	$config = array(
		'views'               => array(
			'modal'           => PAGELESS_BUTTONS_DIR . 'src/views/modal.php',
			'blurb'           => PAGELESS_BUTTONS_DIR . 'src/views/blurb.php',
			'blurb-container' => PAGELESS_BUTTONS_DIR . 'src/views/blurb-container.php',
		),
		'text-color'          => '',
		'button-color'        => '',
		'modal-color'         => '',
		'type'                => '',
		'title'               => '',
		'left-icon'           => '',
		'no-icon'             => '',
		'description'         => '',
		'image'               => '',
		'id'                  => '',
		'background-position' => 'center',
		'background-height'   => '50',
		'button-title'        => '',
		'button-desc'         => '',
	);

	return $config;
}

function render_blurb_posts( array $attributes, array $config ) {

	$config_args = array(
		//number of records to get back
		'posts_per_page' => 6,
		'post_type'  => 'post',
		'order'      => 'DESC',
		'tax_query'  => array(
			'relation' => 'AND',
			array(
				'taxonomy' => 'category',
				'field'    => 'slug',
				'terms'    => array( 'article', 'case study' ),
			),
		),
	);

	$query = new \WP_Query( $config_args );

	if ( ! $query->have_posts() ) {
		return;
	}

	include( $attributes['views']['blurb-container'] );

	wp_reset_postdata();
}

function loop_and_render_blurb( \WP_Query $query, $attributes, $config ) {
	while ( $query->have_posts() ) {
		$query->the_post();

		$attributes['button-title'] = get_the_title();
		$attributes['button-desc']  = get_the_excerpt();
		$attributes['image']        = get_the_post_thumbnail_url();
		$attributes['category']     = get_the_category()[0]->name;
		$attributes['slug']         = get_post_field( 'post_name' );

		$image_id = get_attachment_id_from_url( sanitize_text_field( $attributes['image'] ) );

		include( $config['views'][ $attributes['type'] ] );
	}
}