<?php
/**
 * Plugin Handler
 *
 * @package   ThoriumDesign\PagelessButtons
 * @since     1.0.0
 * @author    ThoriumDesign
 * @link      https://thoriumdesign.com
 * @contributors ajskelton, jtilford
 */

namespace ThoriumDesign\PagelessButtons;

add_action( 'wp_enqueue_scripts', __NAMESPACE__ . '\enqueue_assets' );
/**
 * Enqueue the plugin assets (scripts and styles).
 *
 * @since 1.0.0
 *
 * @return void
 */
function enqueue_assets() {
	wp_enqueue_style( 'dashicons' );
	wp_enqueue_style( 'pageless-buttons-css-styles', PAGELESS_BUTTONS_URL . 'assets/dist/css/index.css', false, PAGELESS_BUTTONS_VERSION, false );
	wp_enqueue_script( 'pageless-buttons-plugin-script', PAGELESS_BUTTONS_URL . 'assets/dist/js/jquery.plugin.js', array( 'jquery' ), PAGELESS_BUTTONS_VERSION, true );
}


/**
 * Add SVG definitions to <head>.
 */
function include_svg_icons() {
	// Define SVG sprite file.
	$svg_icons = PAGELESS_BUTTONS_URL . 'assets/images/svg-icons.svg';
	// If it exsists, include it.
	if ( file_exists( $svg_icons ) ) {
		require_once( $svg_icons );
	}
}

/**
 * Autoload plugin files
 *
 * @since 1.0.0
 *
 * @return void
 */
function autoload() {
	$files = array(
		'shortcode/shortcodes.php',
		'util/utility-functions.php'
	);

	foreach ( $files as $file ) {
		include( __DIR__ . '/' . $file );
	}
}

autoload();