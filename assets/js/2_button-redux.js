jQuery(document).ready(function ($) {
    //trigger the animation - open modal window
    $('[data-type="modal-trigger"]').on('click', function () {
        var actionBtn = $(this),
            modalSize = actionBtn.data('size'),
            scaleValue = retrieveScale(actionBtn.next('.cd-modal-bg'), modalSize),
            modalColor = actionBtn.data('modal-color');

        console.log(modalSize);

        if( 'form' === modalSize || 'slim-form' === modalSize ) {

            var modal = actionBtn.parent(),
                modalContent = modal.siblings('.cd-modal').find('.cd-modal-content'),
                contentPos = modalContent.position(),
                contentHeight = modalContent.height(),
                contentWidth = modalContent.width() + 75,
                borderRadius = '0',
                closeBtn = modal.siblings('.cd-modal-close');
            var btnPos = actionBtn.offset();
            var btnWidthCenter = actionBtn.width() / 2;
            var contentWidthCenter = modalContent.width() / 2;
            var btnHeightCenter = actionBtn.height() / 2;
            var contentHeightCenter = modalContent.height() / 2;

            var modalLeft = btnPos.left + btnWidthCenter - contentWidthCenter;
            var modalTop = btnPos.top + btnHeightCenter - contentHeightCenter;

            console.log(modalLeft);
            contentPos.left = modalLeft;
            contentPos.top = modalTop;

            modalContent.css( 'left', modalLeft );
            // modalContent.css( 'right', modalLeft + contentWidth );
            modalContent.css( 'top', modalTop );
            // modalContent.css( 'bottom', modalTop + contentHeight );
            // console.log(btnWidthCenter, contentWidgthCenter, modalLeft);

            closeBtn.css( 'right', contentPos.left );
            closeBtn.css( 'top', contentPos.top + 35 );
        }
        actionBtn.css('color', 'transparent');
        actionBtn.next('.cd-modal-bg').css('background-color', modalColor);

        actionBtn.addClass('to-circle');
        actionBtn.next('.cd-modal-bg').addClass('is-visible').one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function () {
            modalSize === 'form' || modalSize === 'slim-form' ? animateMediumLayer(actionBtn.next('.cd-modal-bg'), scaleValue, true, contentPos, contentHeight, contentWidth, borderRadius) : animateLayer(actionBtn.next('.cd-modal-bg'), scaleValue, true);
        });

        //if browser doesn't support transitions...
        if (actionBtn.parents('.no-csstransitions').length > 0) animateLayer(actionBtn.next('.cd-modal-bg'), scaleValue, true);
    });

    //trigger the animation - close modal window
    $('.cd-section .cd-modal-close').on('click', function () {
        if($(this).parent().hasClass('cd-form')) {
            closeFormModal();
        } else {
            closeModal();
        }
    });
    $(document).keyup(function (event) {
        var selectorForm = $('.cd-section.cd-form.modal-is-visible');

        if (event.which == '27' && selectorForm.length) {
            closeFormModal();
        } else if (event.which == '27') {
            closeModal();
        }
    });

    $(window).on('resize', function () {
        //on window resize - update cover layer dimention and position
        if ($('.cd-section.cd-full-screen.modal-is-visible').length > 0) {
            window.requestAnimationFrame(updateLayer);
        } else if ($('.cd-section.cd-form.modal-is-visible').length > 0 ) {
            window.requestAnimationFrame(updateFormLayer);
        }
    });

    function retrieveScale(btn, modalSize) {
        var btnRadius = btn.width() / 2,
            left = btn.offset().left + btnRadius,
            top = btn.offset().top + btnRadius - $(window).scrollTop(),
            scale = scaleValue(top, left, btnRadius, $(window).height(), $(window).width()) + 5;

        btn.css('position', 'fixed').velocity({
            top: top - btnRadius,
            left: left - btnRadius,
            translateX: 0,
        }, 0);
        return modalSize === 'form' ? scale / 3 : scale;
    }

    function scaleValue(topValue, leftValue, radiusValue, windowW, windowH) {
        var maxDistHor = ( leftValue > windowW / 2) ? leftValue : (windowW - leftValue),
            maxDistVert = ( topValue > windowH / 2) ? topValue : (windowH - topValue);
        return Math.ceil(Math.sqrt(Math.pow(maxDistHor, 2) + Math.pow(maxDistVert, 2)) / radiusValue);
    }

    function animateLayer(layer, scaleVal, bool) {
        layer.velocity({scale: scaleVal}, 400, function () {
            $('body').toggleClass('overflow-hidden', bool);
            (bool)
                ? layer.parents('.cd-section').addClass('modal-is-visible').end().off('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend')
                : layer.removeClass('is-visible').removeAttr('style').siblings('[data-type="modal-trigger"]').removeClass('to-circle');
        });
    }

    function animateMediumLayer(layer, scaleVal, bool, contentPos, contentHeight, contentWidth, borderRadius) {
        layer.velocity({
            top: contentPos.top,
            left: contentPos.left,
            height: contentHeight,
            width: contentWidth,
            borderRadius: borderRadius,
        }, 400, function () {
            // $('body').toggleClass('overflow-hidden', bool);
            (bool)
                ? layer.parents('.cd-section').addClass('modal-is-visible').end().off('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend')
                : layer.removeClass('is-visible').removeAttr('style').siblings('[data-type="modal-trigger"]').removeClass('to-circle');
        });
    }


    function updateLayer() {
        var layer = $('.cd-section.modal-is-visible').find('.cd-modal-bg'),
            layerRadius = layer.width() / 2,
            layerTop = layer.siblings('.btn').offset().top + layerRadius - $(window).scrollTop(),
            layerLeft = layer.siblings('.btn').offset().left + layerRadius,
            scale = scaleValue(layerTop, layerLeft, layerRadius, $(window).height(), $(window).width()) + 5;

        layer.velocity({
            top: layerTop - layerRadius,
            left: layerLeft - layerRadius,
            scale: scale
        }, 0);
    }

    function updateFormLayer() {
        var layer = $('.cd-section.cd-form.modal-is-visible').find('.cd-modal-bg'),
            modalContent = layer.parent().next().children('.cd-modal-content'),
            contentPos = modalContent.position(),
            contentHeight = modalContent.height(),
            contentWidth = modalContent.width() + 75,
            closeBtn = modalContent.parent().next();

        layer.velocity({
            top: contentPos.top,
            left: contentPos.left,
            height: contentHeight,
            width: contentWidth

        }, 0);

        closeBtn.velocity({
            right: contentPos.left,
            top: contentPos.top + 35
        }, 0);
    }

    function closeModal() {
        var section = $('.cd-section.modal-is-visible'),
            btn = section.find('.btn'),
            btnColor = btn.data('text-color');

        btn.css('color', btnColor);
        section.removeClass('modal-is-visible').one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function () {
            animateLayer(section.find('.cd-modal-bg'), 1, false);
        });
        //if browser doesn't support transitions...
        if (section.parents('.no-csstransitions').length > 0) animateLayer(section.find('.cd-modal-bg'), 1, false);
    }

    function closeFormModal() {
        var section = $('.cd-section.cd-form.modal-is-visible'),
            btn = section.find('.btn'),
            btnColor = btn.data('text-color'),
            btnPos = btn.offset(),
            btnHeight = btn.height(),
            btnWidth = btn.width(),
            borderRadius = '100%';

        console.log(btn);
        btn.css('color', btnColor);
        section.removeClass('modal-is-visible').one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function () {
            animateMediumLayer(section.find('.cd-modal-bg'), 1, false, btnPos, btnHeight, btnWidth, borderRadius);

        })
    }


});

// Defer loading images until after
function init() {
    var imgDefer = document.getElementsByTagName('img');
    for (var i = 0; i < imgDefer.length; i++) {
        if (imgDefer[i].getAttribute('data-src')) {
            imgDefer[i].setAttribute('src', imgDefer[i].getAttribute('data-src'));
        }
    }
}
window.onload = init;