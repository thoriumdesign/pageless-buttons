/*
 Title: Equal Heights
 Description: This code sets the height of several elements which have the same class to the height of the tallest element in the group. It will find all objects with a class of "th-equalize-heights" within a Divi section, and apply equal heights to all nearby siblings. Developed by Jarrett Tilford for Thorium Design, LLC.
 Website: https://thoriumdesign.com
 Author Website: https://jtilford.com
 Version: 1.0.1
 */

jQuery( document ).ready( function($) {

    function equalize_heights_of_siblings() {

        var class_to_equalize = ".th-equalize-heights";

        jQuery(".et_pb_section").each(function (index, value) {
            var the_min_height = 0;
            var $this = jQuery(this);
            var $equalize_these = $this.find(class_to_equalize);

            if ( typeof $equalize_these != "undefined" ) {

                $equalize_these.each(function (i, v) {
                    if ( jQuery(this).find('.modal__trigger').height() > the_min_height ) {
                        the_min_height = jQuery(this).find('.modal__trigger').height();
                    }
                });

                $equalize_these.each(function (i, v) {
                    jQuery(this).find('.modal__trigger').css('min-height', the_min_height);
                });
            }

        });
    }

    equalize_heights_of_siblings();

    jQuery(window).resize( function() {
        // console.log('resize');
            equalize_heights_of_siblings($);
        }
    );

});